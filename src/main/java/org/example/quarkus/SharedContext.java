package org.example.quarkus;

import io.quarkus.arc.Lock;
import io.quarkus.runtime.Startup;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
@Startup
@Lock
public class SharedContext {
    private List<Date> dates;

    SharedContext() {
        dates = new ArrayList<>();
    }

    @Lock(value = Lock.Type.READ, time = 1, unit = TimeUnit.SECONDS)
    public List<Date> getDates() {
        return dates;
    }

    public int addDate(Date date) {
        dates.add(date);
        return dates.size();
    }
}
