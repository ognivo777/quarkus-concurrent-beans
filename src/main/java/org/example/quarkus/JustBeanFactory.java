package org.example.quarkus;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class JustBeanFactory {
    @Inject
    SharedContext context;

    public JustBean newInstance() {
        return new JustBean(context);
    }
}
