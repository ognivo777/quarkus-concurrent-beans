package org.example.quarkus;

import io.smallrye.common.annotation.Blocking;

import javax.enterprise.context.ApplicationScoped;
import java.util.Date;

@ApplicationScoped
public class JustBean {
    SharedContext context;

    private int countAfterAdd;

    public JustBean(SharedContext context) {
        this.context = context;
    }

    @Blocking
    public void add() {
        countAfterAdd = context.addDate(new Date());
    }

    public int getCountAfterAdd() {
        return countAfterAdd;
    }
}
