package org.example.quarkus;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/call")
public class SampleRestEndpoint {

    @Inject
    JustBeanFactory justBeanFactory;

    @GET
    public int call() {
        JustBean justBean = justBeanFactory.newInstance();
        justBean.add();
        return justBean.getCountAfterAdd();
    }
}
